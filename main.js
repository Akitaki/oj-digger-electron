const { app, BrowserWindow } = require('electron');

function createWindow() {
    let win = new BrowserWindow({
        width: 1000,
        height: 620,
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
        },
        show: false,
    });

    win.setMenuBarVisibility(false);
    win.loadFile('setup.html');

    win.once('ready-to-show', () => {
        win.show();
    });
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin')
        app.quit();
});
app.on('activate', () => {
    if (win === null)
        createWindow();
});
