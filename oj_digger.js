const request = require('request-promise');
const cheerio = require('cheerio');
const events = require('events');
const util = require('util');

const nodeSetTimeout = require('electron').remote.getGlobal('setTimeout');
const psleep = (ms) => new Promise(resolve => nodeSetTimeout(resolve, ms));

const token_regex = /name='csrfmiddlewaretoken' value='(.+)'/;
const urls = {
    index: 'https://acm.cs.nthu.edu.tw/',
    login: 'https://acm.cs.nthu.edu.tw/users/login/?next=/',
    submit_problem: (pid) => `https://acm.cs.nthu.edu.tw/users/submit/${pid}`,
    submit: 'https://acm.cs.nthu.edu.tw/users/submit/',
    stat: (username) => `https://acm.cs.nthu.edu.tw/status/?username=${username}`,
};

/**
 * @returns {string}
 */
function read_csrftoken(session_data) {
    const csrf_regex = /csrftoken=(.+); Expires/;
    let cookie_obj = session_data.jar._jar.store
        .idx['acm.cs.nthu.edu.tw']['/']['csrftoken'];
    /** @type {string} */
    let cookie_str = cookie_obj.toString();
    return cookie_str.match(csrf_regex)[1];
}

class OJDigger {
    async touch_login_page(session_data) {
        return request.get(urls.login, {
            jar: session_data.jar,
        })
            .then(res => {})
            .catch(err => {
                console.warn('touch_login_page err');
            })
    }

    async touch_submit_page(pid, session_data) {
        return request.get(urls.submit_problem(pid), {
            jar: session_data.jar,
        })
            .then(res => {})
            .catch(err => {
                console.warn('touch_submit_page err');
            })
    }

    /**
     * @param {SessionData} session_data
     */
    async do_login(session_data) {
        session_data.token = read_csrftoken(session_data);
        return request.post(urls.login, {
            jar: session_data.jar,
            followRedirect: false,
            simple: false,
            headers: {
                'Referer': urls.login,
            },
            form: {
                csrfmiddlewaretoken: session_data.token,
                username: this.username,
                password: this.password,
            },
        })
            .then(res => {})
            .catch(err => {
                console.warn('err', err);
                console.warn('do_login err');
            });
    }

    /**
     * @param {string} pid
     * @param {string} lang
     * @param {string} code
     * @param {SessionData} session_data
     */
    async do_submit(pid, lang, code, session_data) {
        session_data.token = read_csrftoken(session_data);
        console.log('[Digger] do_submit token', session_data.token);
        console.log('[Digger] do_submit referer', urls.submit_problem(pid));

        return request.post(urls.submit, {
            jar: session_data.jar,
            followRedirect: false,
            simple: false,
            headers: {
                'Referer': urls.submit_problem(pid),
            },
            form: {
                csrfmiddlewaretoken: session_data.token,
                pid: pid,
                language: lang,
                code: code,
            },
        })
            .then(res => {
                console.log('[Digger] do_submit res');
            })
            .catch(err => {
                console.warn('[Digger] do_submit err');
            })
    }

    /**
     * @returns {Promise.<string>}
     */
    async do_stat() {
        return request.get(urls.stat(this.username), {})
            .then(res => {
                console.log('[Digger] do_stat res');
                return res;
            })
            .catch(err => {
                console.warn('[Digger] do_stat err');
            })
    }

    async wait_til_judged() {
        return psleep(2000)
            .then(() => this.do_stat())
            .then(res => {
                let test_str = res
                    .replace('<option value="JUDGING">Judging</option>', '')
                    .replace('<option value="WAIT">Being Judged</option>', '');
                if (test_str.includes("Judging") || test_str.includes("Being Judged")) {
                    console.log('[Digger] not judged. wait for 3 sec...');
                    this.event.emit('stillwait');
                    return this.wait_til_judged();
                }
                return res;
            });
    }

    async parse_status(res, from_bit, to_bit, step_bit) {
        const $ = cheerio.load(res);
        let all_results = []; // single char

        $('tbody tr')
            .slice(0, (to_bit - from_bit) / step_bit + 1)
            .each((i, el) => {
                let title = $(el).attr('title');
                if (!title)
                    return;

                let lines = title
                    .split('\n')
                    .filter(str => str.includes('<li'))
                    .map(str => str.trim());

                let regex = /row'>(.)/;
                let results = lines.map(el => el.match(regex)[1]);
                all_results.push(results);
            });
        return all_results;
    }

    async retrieve_result(from_bit, to_bit, step_bit) {
        const mapping = (ch) => {
            switch (ch) {
                case 'W':
                    return '0';
                case 'R':
                    return '1';
                default:
                    return '?';
            }
        }

        this.event.emit('waiting');
        return this.wait_til_judged()
            .then(res => this.parse_status(res, from_bit, to_bit, step_bit))
            .then(all_results => {
                if (all_results.length === 0) {
                    console.error('[Digger] no results, aborting');
                    throw new Error('all_results length is zero');
                }

                console.log('[Digger] all results:')
                console.log(all_results);
                
                let testcase_count = all_results[0].length;
                let bit_strings = Array(testcase_count).fill('');

                for (let results of all_results) {
                    for (let i = 0; i < testcase_count; i++)
                        bit_strings[i] += mapping(results[i])
                }

                console.log('[Digger] bit_strings:')
                console.log(bit_strings);
                this.event.emit('done');
                return bit_strings;
            });
    }

    /**
     * @typedef {Object} SessionData
     * @property {any} jar
     * @property {string} token
     */

    code_with_shift(shift) {
        return this.template.replace('$SHIFT', shift);
    }

    async submit_with_shift(shift, session_data) {
        return this.touch_submit_page(this.pid, session_data)
            .then(() => this.do_submit(this.pid, this.lang,
                this.code_with_shift(shift), session_data)
            );
    }
    
    async submit_all(from_bit, to_bit, step_bit, session_data) {
        for (let i = from_bit; i <= to_bit; i += step_bit) {
            this.event.emit('submit',
                `${i}/${(to_bit - from_bit) / step_bit + 1}`,
                i,
                (to_bit - from_bit) / step_bit + 1
            );
            await this.submit_with_shift(i - 1, session_data);
        }
    }
 
    async start_digging(from_bit, to_bit, step_bit) {
        /** @type {SessionData} */
        let session_data = {
            jar: request.jar(),
            token: '',
        }

        // Login
        return this.touch_login_page(session_data)
            .then(() => this.do_login(session_data))
            .then(() => {
                this.event.emit('login');
            })
            .then(() => this.submit_all(from_bit, to_bit, step_bit, session_data));
    }

    /**
     * @param {('login'|'submit'|'waiting'|'stillwait'|'done')} name
     * @param {any} callback
     */
    on(name, callback) {
        this.event.on(name, callback);
    }

    /**
     * @typedef {Object} OJDiggerOptions
     * @property {string} username
     * @property {string} password
     * @property {string} pid
     * @property {string} lang
     * @property {string} template
     */

    /**
     * @param {OJDiggerOptions} options
     */
    constructor({username, password, pid, lang, template}) {
        this.username = username;
        this.password = password;
        this.pid = pid;
        this.lang = lang;
        this.template = template;

        this.event = new events.EventEmitter();
    }
}

module.exports = OJDigger;
